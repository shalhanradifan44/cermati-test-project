
//GET HTML TAG, CAN BE CHANGED WITH BODY
var html = document.documentElement; 
//HTML HEIGHT WITH SCROLL
var fullHeight = html.scrollHeight;
//HTML HEIGHT ON SCREEN (WITHOUT SCROLL)
var screenHeight = html.clientHeight;
//SLIDER
var slider = document.getElementById("slider");
//SLIDER CLOSE COUNTER
var isClosed = false;


//When Window Has No Scroll
setTimeout(()=> {
    showSlider();
},1000)

//When Window Has Scroll
function scrollDetection() {
    showSlider();
}

//Show Slider
function showSlider() {
    if(!isClosed) {
        //SCROLL POSITION
        var scrollHeight = html.scrollTop;
        //INSERT CLASS 'IN'
        if(!slider.classList.contains("in")){
            if((fullHeight <= screenHeight && scrollHeight == 0)   || fullHeight / 2 < scrollHeight * 2){
                slider.classList.add("in");
            }
        }
    }
}

//hide slider
function hideSlider() {
    isClosed = true;
    slider.classList.remove("in");
    setTimeout(()=> {
        isClosed = false;
    },1000 * 10 * 60);
} 
